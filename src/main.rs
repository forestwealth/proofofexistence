// Copyright 2017 The Exonum Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Import crates with necessary types into a new project.

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate exonum;
extern crate time;
extern crate router;
extern crate bodyparser;
extern crate iron;

// Import necessary types from crates.

use exonum::blockchain::{Blockchain, Service, GenesisConfig, ValidatorKeys, Transaction,
                         ApiContext};
use exonum::node::{Node, NodeConfig, NodeApiConfig, TransactionSend, ApiSender};
use exonum::messages::{RawTransaction, FromRaw};
use exonum::storage::{Fork, MemoryDB, MapIndex};
use exonum::crypto::{verify, PublicKey, Hash, Signature, HexValue};
use exonum::encoding;
use exonum::api::{Api, ApiError};
use iron::prelude::*;
use iron::Handler;
use router::Router;

// // // // // // // // // // CONSTANTS // // // // // // // // // //

// Define service ID for the service trait.

const SERVICE_ID: u16 = 1;

// Define constants for transaction types within the service.

const TX_CREATE_POE_ID: u16 = 1;

const TRUSTED_PUBLIC_KEY: [u8; 32] = [9, 236, 52, 118, 86, 111, 121, 255, 52, 16, 185, 47, 36, 8, 15, 208, 158, 229, 102, 138, 144, 224, 47, 73, 227, 168, 89, 129, 146, 232, 64, 55];

// // // // // // // // // // PERSISTENT DATA // // // // // // // // // //

// Declare the data to be stored in the blockchain. In the present case,
// declare a type for storing information about the poe and its balance.

/// Declare a [serializable][1]
/// [1]: https://github.com/exonum/exonum-doc/blob/master/src/architecture/serialization.md
/// struct and determine bounds of its fields with `encoding_struct!` macro.
encoding_struct! {
    struct ProofOfExistence {
        const SIZE = 104;

        field hash_:           &Hash       [00 => 32]
        field signature:       &Signature  [32 => 96]
        field ts:              i64         [96 => 104]
    }
}

// // // // // // // // // // DATA LAYOUT // // // // // // // // // //

/// Create schema of the key-value storage implemented by `MemoryDB`. In the
/// present case a `Fork` of the database is used.
pub struct PoeSchema<'a> {
    view: &'a mut Fork,
}

/// Declare layout of the data. Use an instance of [`MapIndex`][2]
/// [2]: https://github.com/exonum/exonum-doc/blob/master/src/architecture/storage.md#mapindex
/// to keep poes in storage. Index values are serialized `Poe` structs.
///
/// Isolate the poes map into a separate entity by adding a unique prefix,
/// i.e. the first argument to the `MapIndex::new` call.
impl<'a> PoeSchema<'a> {
    pub fn poes(&mut self) -> MapIndex<&mut Fork, Hash, ProofOfExistence> {
        MapIndex::new("forestwealth.poe", self.view)
    }

    /// Get a separate poe from the storage.
    pub fn poe(&mut self, hash: &Hash) -> Option<ProofOfExistence> {
        self.poes().get(hash)
    }
}

// // // // // // // // // // TRANSACTIONS // // // // // // // // // //

/// Create a new poe.
message! {
    struct TxCreatePoe {
        const TYPE = SERVICE_ID;
        const ID = TX_CREATE_POE_ID;
        const SIZE = 96;

        field hash:            &Hash       [00 => 32]
        field signature:       &Signature  [32 => 96]
    }
}

// // // // // // // // // // CONTRACTS // // // // // // // // // //

/// Execute a transaction.
impl Transaction for TxCreatePoe {
    /// Verify integrity of the transaction by checking the transaction
    /// signature.
    fn verify(&self) -> bool {
        // FIXME: typed public key is recreated on every call
        let trusted_pubkey = PublicKey::new(TRUSTED_PUBLIC_KEY);
        verify(self.signature(), self.hash().as_ref(), &trusted_pubkey)
    }

    /// Apply logic to the storage when executing the transaction.
    fn execute(&self, view: &mut Fork) {
        let mut schema = PoeSchema { view };
        if schema.poe(self.hash()).is_none() {
            let ts = time::now_utc().to_timespec().sec;
            let poe = ProofOfExistence::new(self.hash(), self.signature(), ts);
            println!("Create PoE: {:?}", poe);
            schema.poes().put(self.hash(), poe)
        }
    }

    /// Provide information about the transaction to be used in the blockchain explorer.
    fn info(&self) -> serde_json::Value {
        serde_json::to_value(&self).expect("Cannot serialize transaction to JSON")
    }
}

// // // // // // // // // // REST API // // // // // // // // // //

/// Implement the node API.
#[derive(Clone)]
struct PoeApi {
    channel: ApiSender,
    blockchain: Blockchain,
}

/// Shortcut to get data on poes.
impl PoeApi {
    fn get_poe(&self, hash: &Hash) -> Option<ProofOfExistence> {
        let mut view = self.blockchain.fork();
        let mut schema = PoeSchema { view: &mut view };
        schema.poe(hash)
    }

    fn get_poes(&self) -> Option<Vec<ProofOfExistence>> {
        let mut view = self.blockchain.fork();
        let mut schema = PoeSchema { view: &mut view };
        let idx = schema.poes();
        let poes: Vec<ProofOfExistence> = idx.values().collect();
        if poes.is_empty() {
            None
        } else {
            Some(poes)
        }
    }
}

/// Add an enum which joins transactions of both types to simplify request
/// processing.
#[serde(untagged)]
#[derive(Clone, Serialize, Deserialize)]
enum TransactionRequest {
    CreatePoe(TxCreatePoe),
}

/// Implement a trait for the enum for deserialized `TransactionRequest`s
/// to fit into the node channel.
impl Into<Box<Transaction>> for TransactionRequest {
    fn into(self) -> Box<Transaction> {
        match self {
            TransactionRequest::CreatePoe(trans) => Box::new(trans),
        }
    }
}

/// The structure returned by the REST API.
#[derive(Serialize, Deserialize)]
struct TransactionResponse {
    tx_hash: Hash,
}

/// Implement the `Api` trait.
/// `Api` facilitates conversion between transactions/read requests and REST
/// endpoints; for example, it parses `POSTed` JSON into the binary transaction
/// representation used in Exonum internally.
impl Api for PoeApi {
    fn wire(&self, router: &mut Router) {
        let self_ = self.clone();
        let transaction = move |req: &mut Request| -> IronResult<Response> {
            match req.get::<bodyparser::Struct<TransactionRequest>>() {
                Ok(Some(transaction)) => {
                    let transaction: Box<Transaction> = transaction.into();
                    let tx_hash = transaction.hash();
                    self_.channel.send(transaction).map_err(ApiError::from)?;
                    let json = TransactionResponse { tx_hash };
                    self_.ok_response(&serde_json::to_value(&json).unwrap())
                }
                Ok(None) => Err(ApiError::IncorrectRequest("Empty request body".into()))?,
                Err(e) => Err(ApiError::IncorrectRequest(Box::new(e)))?,
            }
        };

        // Gets status of all poes.
        let self_ = self.clone();
        let poes_info = move |_: &mut Request| -> IronResult<Response> {
            if let Some(poes) = self_.get_poes() {
                self_.ok_response(&serde_json::to_value(poes).unwrap())
            } else {
                self_.not_found_response(
                    &serde_json::to_value("Poes database is empty")
                        .unwrap(),
                )
            }
        };

        // Gets status of the poe corresponding to the public key.
        let self_ = self.clone();
        let poe_info = move |req: &mut Request| -> IronResult<Response> {
            let path = req.url.path();
            let poe_hash = path.last().unwrap();
            let hash = Hash::from_hex(poe_hash).map_err(ApiError::FromHex)?;
            if let Some(poe) = self_.get_poe(&hash) {
                self_.ok_response(&serde_json::to_value(poe).unwrap())
            } else {
                self_.not_found_response(&serde_json::to_value("Poe not found").unwrap())
            }
        };

        // Bind the transaction handler to a specific route.
        router.post("/v1/poe/transaction", transaction, "transaction");
        router.get("/v1/poes", poes_info, "poes_info");
        router.get("/v1/poe/:hash", poe_info, "poe_info");
    }
}

// // // // // // // // // // SERVICE DECLARATION // // // // // // // // // //

/// Define the service.
struct PoeService;

/// Implement a `Service` trait for the service.
impl Service for PoeService {
    fn service_name(&self) -> &'static str {
        "cryptocurrency"
    }

    fn service_id(&self) -> u16 {
        SERVICE_ID
    }

    /// Implement a method to deserialize transactions coming to the node.
    fn tx_from_raw(&self, raw: RawTransaction) -> Result<Box<Transaction>, encoding::Error> {
        let trans: Box<Transaction> = match raw.message_type() {
            TX_CREATE_POE_ID => Box::new(TxCreatePoe::from_raw(raw)?),
            _ => {
                return Err(encoding::Error::IncorrectMessageType {
                    message_type: raw.message_type(),
                });
            }
        };
        Ok(trans)
    }

    /// Create a REST `Handler` to process web requests to the node.
    fn public_api_handler(&self, ctx: &ApiContext) -> Option<Box<Handler>> {
        let mut router = Router::new();
        let api = PoeApi {
            channel: ctx.node_channel().clone(),
            blockchain: ctx.blockchain().clone(),
        };
        api.wire(&mut router);
        Some(Box::new(router))
    }
}

// // // // // // // // // // ENTRY POINT // // // // // // // // // //

fn main() {
    exonum::helpers::init_logger().unwrap();

    println!("Creating in-memory database...");
    let db = MemoryDB::new();
    let services: Vec<Box<Service>> = vec![Box::new(PoeService)];
    let blockchain = Blockchain::new(Box::new(db), services);

    let (consensus_public_key, consensus_secret_key) = exonum::crypto::gen_keypair();
    let (service_public_key, service_secret_key) = exonum::crypto::gen_keypair();

    let validator_keys = ValidatorKeys {
        consensus_key: consensus_public_key,
        service_key: service_public_key,
    };
    let genesis = GenesisConfig::new(vec![validator_keys].into_iter());

    let api_address = "0.0.0.0:8000".parse().unwrap();
    let api_cfg = NodeApiConfig {
        public_api_address: Some(api_address),
        ..Default::default()
    };

    let peer_address = "0.0.0.0:2000".parse().unwrap();

    let node_cfg = NodeConfig {
        listen_address: peer_address,
        peers: vec![],
        service_public_key,
        service_secret_key,
        consensus_public_key,
        consensus_secret_key,
        genesis,
        external_address: None,
        network: Default::default(),
        whitelist: Default::default(),
        api: api_cfg,
        mempool: Default::default(),
        services_configs: Default::default(),
    };

    println!("Starting a single node...");
    let node = Node::new(blockchain, node_cfg);

    println!("Blockchain is ready for transactions!");
    node.run().unwrap();
}
